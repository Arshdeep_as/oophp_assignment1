<?php
/**
 * App Class
 * Front Controller Class for Assignment 1
 */

namespace App;

class App
{

	/**
	 * IOC container
	 */
	private $c;

	/**
	 * @param Pimple $container
	 */
	public function __construct(\Pimple\Container $container)
	{
		$this->c = $container;
	}

	/**
	 * Run the app
	 */
	public function run()
	{
		$c = $this->c;
		$this->log($c['logger']);
	}

	/**
	 * Log requests
	 */
	public function log(\App\Lib\ILogger $logger)
	{
		//concating date, time, serveer adderss, servername, request_method, http response code and php self to the $event string.
		
		$event = date("Y:m:d")." ".date("h:i:s").' | '. $_SERVER['SERVER_ADDR'].' | '.$_SERVER['SERVER_NAME'] . ' | ' .$_SERVER['REQUEST_METHOD']. ' | ' . http_response_code() . ' | ' .$_SERVER['PHP_SELF'];


		/* DO NOT EDIT BELOW THIS LINE
		--------------------------------------------------- */

		echo "<p>Logged using " . get_class($logger) . ":</p>";
		var_dump($event);
		$logger->write($event);
		
	}


}
