<?php

namespace App\Lib;

class DatabaseLogger implements ILogger
{	
	//creating a private variable to store the database handler.
	private $dbh;
	
	/**
	 * Constructor method to initialize the private dbh.
	 * @param [object] $file the path of the file to which the contents needs to br written
	 */
	public function __construct($pdo)
	{
		$this->dbh = $pdo;
	}
	
	/**
	 * write method to write the content to the database.
	 * @param  [String] $event the event string which contains the data that needs to be inserted to the database
	 * @return [void]
	 */
	public function write($event){
		//creating insert query
		$query = 'INSERT INTO events (event) VALUES (:event)';

		//prepare the statement bby using this keyword, by using the dbh.
		$stmt = $this->dbh->prepare($query);
    
    	//binding values, used '\PDO' because, we are use=ing namespace, if we dont use it, browser will look for it in App/Lib folde but we want to look it for in the root folder, thatswhy we use \PDO
        $stmt->bindValue(':event', $event, \PDO::PARAM_STR);
        
        //execute the query.
        $stmt->execute();
      

	}
}