<?php
/**
 * Created by steve
 * Date: 16-09-30
 * Time: 1:08 PM
 */

namespace App\Lib;

interface ILogger
{

	public function write($event);

}