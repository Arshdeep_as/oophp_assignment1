<?php

namespace App\Lib;

class FileLogger implements ILogger
{
	//creating private variable file, to store the path of the file to which we have to write the $event string.
	private $file;

	/**
	 * constructor to initialize the file property.
	 * @param [object] $file the path of the file to which the contents needs to br written
	 */
	public function __construct( $file)
	{
		$this->file = $file;
	}

	/**
	 * write method to write the content to the file.
	 * @param  [String] $event the event string which contains the data that needs to be inserted to the file
	 * @return [void]
	 */
	public function write($event)
	{
		//var_dump($event);
		//creating a handler object, to handle(open, closing, writng) the file, passing it the address of the file and mode in which this file need to be opened, '+a' in appended mode. open function is used to open the file.
		$handle = fopen($this->file, "a+");
		//write function to write $event sting to the file.
		fwrite($handle, $event);
		fclose($handle);
	}
}