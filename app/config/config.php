<?php

return array (
	'log_file' => __DIR__ . '/../storage/event.log',
	'sqlite_file' => __DIR__ . '/../storage/log.sqlite',
	'db_dsn' => 'mysql:host=localhost;dbname=log',
	'db_user' => 'web_user',
	'db_pass' => 'mypass'
);