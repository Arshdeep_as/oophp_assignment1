<?php

/* YOU ONLY NEED TO MODIFY ONE LINE IN THIS FILE FOR TESTING */

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);
session_start();
ob_start();

require __DIR__ . '/../vendor/autoload.php';

use App\App;
use App\Lib\DatabaseLogger;
use App\Lib\FileLogger;
use Pimple\Container;

// Load config info
$config = require __DIR__ . '/../app/config/config.php';

// Create IOC container
$c = new Container($config);

// Add a mysql connection for Database Logger if used
$c['mysql_conn'] = function() use($c) {
    return new \PDO($c['db_dsn'], $c['db_user'], $c['db_pass']);
};

// Add an sqlite connection for Database Logger if used
$c['sqlite_conn'] = function() use($c) {
    return new \PDO('sqlite:' . $c['sqlite_file']);
};

/* Use one of these loggers
----------------------------------------------------------- */

// SQLite logger
$sqlite_logger = function() use ($c)
{
	return new DatabaseLogger($c['sqlite_conn']);
};

// MySQL logger
$mysql_logger = function() use ($c)
{
	return new DatabaseLogger($c['mysql_conn']);
};

// File logger
$file_logger = function() use ($c)
{
	return new FileLogger($c['log_file']);
}; 




// Pick one of the above loggers: $file_logger, $mysql_logger, $sqlite_logger

$c['logger'] = $sqlite_logger;



// Instantiate a new app with all config and dependencies
$app = new App($c);

// Run the app
$app->run();



